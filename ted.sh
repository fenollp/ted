#!/bin/bash

usage() {
    echo "Usage: $0  ‹Destination Folder› ‹TED talk's URL› [ ‹TED talk's URL› ]⁺"
    echo "Requires: wget."
    exit 1
}
[[ $# -lt 2 ]] && usage

print () {
    #return # Sets verbose off.
    echo -ne "\033[01;34m$1"
    shift 1
    echo -e " :: \033[00m\033[01;01m$*\033[00m"
}

trim_sides() {
    [[ $# -ne 3 ]] && exit 1
    overhead=$1
    overfoot=$3
    word=$2
    echo ${word:${#overhead}:$((- ${#overhead} + ${#word} - ${#overfoot}))}
}

UA="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11"

D="$1"

until [ "$2" = '' ]; do
    page="$2"

    F="${D}/tmp_${RANDOM}.htm"
    print "Destination Folder" $D

    print "Page" $page
    wget --quiet -O $F --user-agent="${UA}" $page

    if [ ! -f $F ]; then
        echo "Pas pu accéder à la vidéo"
        echo "…ou alors l'ordre des arguments est mauvais"
        usage
    fi

    id=$( cat $F | grep -Eo 'com/talks/[^/\.]+\.mp4' )
    id=$( trim_sides 'com/talks/' $id '.mp4' )
    url=http://download.ted.com/talks/$id-480p.mp4?apikey=TEDDOWNLOAD
    print "Stream Remote Address" $url

    name=$( cat $F | grep -Eo '<title>[^<|]+' )
    echo .$name.
    name=$( trim_sides '<title>' "$name" ' ' | sed 's/:/,/g' )
    echo .$name.
    print "Title" "$name"

    rm $F
    fn="${D}/${name} - TED.com".mp4
    wget -O "$fn" --user-agent="${UA}" $url
    print "File Saved at" "$fn"
    echo -e "$id\t$(date)\t$url" >> ~/.dled

    shift
done
