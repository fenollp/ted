<!-- TED API: http://www.ted.com/download/links/slug/BillGates_2010/type/talks/ext/mp4 -->

	<p>Right-click (or control-click on a Mac) to ensure download.</p>
	<div class="column">
		<h3>Audio</h3>
		<dl id="audio_downloads" class="downloads">
							<dt><a href="http://download.ted.com/talks/BillGates_2010.mp3?apikey=TEDDOWNLOAD">Download to desktop (MP3)</a></dt>
				<dd>A specially edited audio version of this talk.</dd>
									<dt><a href="itpc://www.ted.com/itunes/mp3/talks/BillGates_2010">Audio to iTunes (MP3)</a></dt>
					<dd>Click to download directly to your iTunes library.</dd>
									</dl>
	</div>
	<div class="column">
		<h3>Video</h3>
		<form id="video_downloads">
			<dl class="downloads">
									<dt><em>Video downloads are available in mp4 format. Select the download options that best suit your needs.</em></dt>
											<dt><label for="download_subtitles"><strong>Subtitles</strong></label>:</dt>
						<dd><select name="download_subtitles" id="download_subtitles"><option value="">Off</option>
<option value="sq">Albanian</option>
<option value="ar">Arabic</option>
<option value="bg">Bulgarian</option>
<option value="ca">Catalan</option>
<option value="zh-cn">Chinese, Simplified</option>
<option value="zh-tw">Chinese, Traditional</option>
<option value="hr">Croatian</option>
<option value="cs">Czech</option>
<option value="nl">Dutch</option>
<option value="en">English</option>
<option value="fr">French</option>
<option value="de">German</option>
<option value="el">Greek</option>
<option value="gu">Gujarati</option>
<option value="he">Hebrew</option>
<option value="hu">Hungarian</option>
<option value="id">Indonesian</option>
<option value="it">Italian</option>
<option value="ja">Japanese</option>
<option value="ko">Korean</option>
<option value="ms">Malay</option>
<option value="mn">Mongolian</option>
<option value="fa">Persian</option>
<option value="pl">Polish</option>
<option value="pt">Portuguese</option>
<option value="pt-br">Portuguese, Brazilian</option>
<option value="ro">Romanian</option>
<option value="ru">Russian</option>
<option value="sr">Serbian</option>
<option value="sk">Slovak</option>
<option value="es">Spanish</option>
<option value="th">Thai</option>
<option value="tr">Turkish</option>
<option value="ur">Urdu</option>
<option value="vi">Vietnamese</option>
</select></dd>
										<dt><strong>Quality:</strong></dt>
					<dd>
						<input name="download_quality" id="download_quality_light" type="radio" data-name="light" value="-light"  /> <label for="download_quality_light">Low-res</label>
					</dd><dd>
						<input name="download_quality" id="download_quality_regular" type="radio" data-name="regular" value="" checked="checked" /> <label for="download_quality_regular">Standard-res</label>
					</dd><dd>
						<input name="download_quality" id="download_quality_480p" type="radio" data-name="480p" value="-480p"  /> <label for="download_quality_480p">High-res</label>
					</dd>											<dt><strong>Save video file to:</strong></dt>
						<dd><input name="download_location" id="download_location_desktop" type="radio" value="desktop" checked="checked" /> <label for="download_location_desktop">Desktop</label></dd>
						<dd><input name="download_location" id="download_location_itunes" type="radio" value="itunes" /> <label for="download_location_itunes">iTunes</label></dd>
										<dt><a id="download_url" href="">Download &raquo;</a></dt>
					<dd><em>Right-click (or control-click on a Mac) to ensure download.</em></dd>
							</dl>
		</form>
	</div>
<script type="text/javascript">//<![CDATA[
$(function() {
	$('#audio_downloads a').click(function() {
		$.GA_trackEvent('download', $(this).text(), 'BillGates_2010');
	});
	$('#video_downloads').submit(function(event) {
		event.preventDefault();
		set_download_form();
		$('#download_url').click();
	});
	var download_event = { slug: 'BillGates_2010' };
	$('#download_url').mousedown(function(event) {
		$.GA_trackEvent(
			'download',
			download_event.location + '/' + download_event.quality + (download_event.langCode ? '/' + download_event.langCode : ''),
			download_event.slug
		);
	});
	var form = $('#video_downloads');
	form.find('select, input').change(set_download_form);
	function set_download_form() {
		var lang     = form.find('#download_subtitles');
		var langCode = lang.length ? lang.val() : '';
		// low res has a different suffix for subtitled podcasts
		form.find('#download_quality_light')
			.val(langCode ? '-low' : '-light')
			.data('name', langCode ? 'low' : 'light');
		var quality  = form.find('input[name=download_quality]:checked');
		var location = form.find('input[name=download_location]:checked');
		var standard = form.find('#download_quality_regular');

		if (lang.length > 0 && lang.val() != '') {
			standard.parent().slideUp();
			if (standard.prop('checked')) {
				form.find('input[name=download_quality]:first').click();
				return set_download_form();
			}
		} else {
			standard.parent().slideDown();
		}

		if (location.val() == 'itunes') {
						var url = 'itpc://' + document.location.host + '/download/podcast/type/talks/slug/BillGates_2010/ext/mp4/quality/{quality}/lang/{lang}/sf_format/xml'
				.replace('{quality}', quality.data('name'))
				.replace('{lang}', langCode);
			if (langCode == '') url = url.replace('/lang/', '');
		} else {
			var url = 'http://download.ted.com/talks/BillGates_2010{quality}{lang}.mp4?apikey=TEDDOWNLOAD'
				.replace('{quality}', quality.val())
				.replace('{lang}', langCode ? '-' + langCode : '');
		}
		$('#download_url').attr('href', url);
		download_event.location = location.val();
		download_event.quality  = quality.data('name');
		download_event.langCode = langCode;
	}
	set_download_form();
});
//]]></script>
