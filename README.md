#TEDtalks videos backup
A UNIX Shell script that accepts command-line requests to download
`www.ted.com`'s talks.

##Synopsis

    :::bash
    ./ted.sh  ‹destinating folder› ‹TED talk's URL› [ ‹TED talk's URL› ]⁺

###Case Study

Here you have a show you would like to backup, pick its address. It should look
like `http://www.ted.com/talks/bill_gates.html`.

Download the show with:

    :::bash
    ./ted.sh ~/Downloads http://www.ted.com/talks/bill_gates.html

##Requirements
* wget
